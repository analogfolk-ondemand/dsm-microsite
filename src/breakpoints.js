// Screen sizes
const sizes = {
  xs: 320,
  sm: 576,
  md: 768,
  lg: 992,
  lg2: 1200,
  xl: 1400,
  xxl: 1600,
};

// Breakpoints based on sizes config
export const breakpoints = {
  xs:     `@media (min-width: ${sizes.xs}px) and (max-width: ${sizes.md - 1}.98px)`,
  sm:     `@media (min-width: ${sizes.sm}px)`,
  md:     `@media (min-width: ${sizes.md}px)`,
  mdOnly: `@media (min-width: ${sizes.md}px) and (max-width: ${sizes.xl - 1}.98px)`,
  mdDown: `@media (min-width: ${sizes.xs}px) and (max-width: ${sizes.xl - 1}.98px)`,
  lg:     `@media (min-width: ${sizes.lg}px)`,
  lgDown: `@media (min-width: ${sizes.xs}px) and (max-width: ${sizes.xxl - 1}.98px)`,  
  lg2:    `@media (min-width: ${sizes.lg2}px)`,
  lg2Down:`@media (min-width: ${sizes.xs}px) and (max-width: ${sizes.lg2 - 1}.98px)`,  
  xl:     `@media (min-width: ${sizes.xl}px)`,
  xlOnly: `@media (min-width: ${sizes.xl}px) and (max-width: ${sizes.xxl - 1}.98px)`,
  xxl:    `@media (min-width: ${sizes.xxl}px)`,
};
