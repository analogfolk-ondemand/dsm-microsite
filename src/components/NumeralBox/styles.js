import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import ContentBox from "../ContentBox/ContentBox";

export const NumeralBoxWrapper = styled(ContentBox)`
  border: 3px solid var(--color-white);
  box-shadow: inset 1px 2px 0 1px #c8bfbf, 0px 2px 0 1px #c8bfbf;
  ${breakpoints.xs} {
    flex-direction: column;
  }

  .box-numeral__content-number {
    ${breakpoints.md} {
      overflow: hidden;
      text-align: center;
      width: 250px;
    }
    .number {
      font-size: 9.75rem;
      font-weight: bold;
      letter-spacing: -0.85px;
      text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
      ${breakpoints.md} {
        font-size: 10.9rem;
        letter-spacing: -0.98px;
      }
      ${breakpoints.mdDown} {
        display: inline-block;
        overflow: visible;
        position: relative;
        text-align: center;
        width: 142px;
      }
      ${breakpoints.xxl} {
        font-size: 11.25rem;
      }
    }

    .percentage {
      -webkit-text-stroke: 1px var(--color-white);
      color: transparent;
      font-size: 5.563rem;
      font-weight: 400;
      letter-spacing: -0.48px;
    }
  }

  .box-numeral__divider {
    background-color: var(--color-white);
    border-right: 2px solid #c8bfbf;
    display: inline-block;
    height: 4px;
    margin: 0 30px;
    width: 203px;
    ${breakpoints.md} {
      height: 176px;
      width: 3px;
    }
    ${breakpoints.xxl} {
      width: 4px;
    }
  }

  .box-numeral__content-right {
    font-size: 1rem;
    font-weight: 600;
    letter-spacing: -0.41px;
    line-height: 27px;
    max-width: 418px;
    text-transform: uppercase;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
    ${breakpoints.xs} {
      margin-top: 48px;
      text-align: center;
    }
    ${breakpoints.md} {
      font-size: 1.08rem;
      letter-spacing: -0.47px;
    }
    ${breakpoints.xxl} {
      font-size: 1.12rem;
    }
  }
`;
