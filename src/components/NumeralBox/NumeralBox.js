import React, { useState, useEffect, useRef, useContext } from "react";
import Parse from "html-react-parser";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { PageDataContext } from "../../App";
import { VisibilityObserver } from "reactjs-visibility";
import { NumeralBoxWrapper } from "./styles";

export default function NumeralBox({ number, text, animationDelay }) {
  const [isVisible, setIsVisible] = useState(false);
  const { isMobile } = useContext(PageDataContext);
  const numberRef = useRef();

  const handleChangeVisibility = (visible) => {
    if (visible) {
      setIsVisible(true);
      numberRef.current.innerHTML = "0";
    }
  };

  const animateValue = (obj, start, end, duration) => {
    let startTimestamp = null;
    const step = (timestamp) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);
      obj.innerHTML = Math.floor(progress * (end - start) + start);

      if (progress < 1) {
        window.requestAnimationFrame(step);
      }
    };

    window.requestAnimationFrame(step);
  };

  useEffect(() => {
    const animateInNumbers = () => {
      if (isVisible === true) {
        numberRef.current.parentNode.style.opacity = 1;

        if (isMobile) {
          animateValue(numberRef.current, 0, number, 700);
        } else {
          setTimeout(
            () => {
              animateValue(numberRef.current, 0, number, 700);
            },
            animationDelay ? animationDelay : 1500
          );
        }

        setIsVisible(false);
      }
    };

    animateInNumbers();
  }, [isVisible, animationDelay, isMobile, number]);

  return (
    <NumeralBoxWrapper className="box-numeral">
      <div className="box-numeral__content-number">
        <VisibilityObserver onChangeVisibility={handleChangeVisibility} />
        <span className="number" ref={numberRef}>
          0
        </span>
        <span className="percentage">%</span>
      </div>
      <AnimationOnScroll
        animateIn="animate__fadeInUp"
        delay={isMobile ? 0 : 700}
      >
        <div className="box-numeral__divider"></div>
      </AnimationOnScroll>
      <AnimationOnScroll
        animateIn="animate__fadeInUp"
        delay={isMobile ? 0 : 800}
      >
        <div className="box-numeral__content-right">{Parse(text)}</div>
      </AnimationOnScroll>
    </NumeralBoxWrapper>
  );
}
