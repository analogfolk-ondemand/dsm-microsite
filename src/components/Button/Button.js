import React from "react";
import { AnchorWrapper, ButtonWrapper } from "./styles";

export default function Button({ id, text, link, className, onClick }) {
  if (link) {
    return (
      <AnchorWrapper
        id={id ? id : ""}
        className={`primary-button ${className ? className : ""}`}
        href={link}
        target="_blank"
        rel="noreferrer"
        onClick={onClick}
      >
        {text}
      </AnchorWrapper>
    );
  }
  return (
    <ButtonWrapper
      id={id ? id : ""}
      className={`primary-button ${className ? className : ""}`}
    >
      {text}
    </ButtonWrapper>
  );
}
