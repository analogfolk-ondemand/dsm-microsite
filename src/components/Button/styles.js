import styled from "styled-components";
import { breakpoints } from "../../breakpoints";

const styles = `
  border: 1px solid var(--color-white);
  display: block;
  font-size: 0.94rem;
  font-weight: 600;
  height: 62px;
  letter-spacing: normal;
  line-height: 62px;
  text-align: center;
  text-decoration: none;
  text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
  transition: background-color 0.5s;
  width: 240px;
  ${breakpoints.xxl} {
    font-size: 1rem;
    height: 64px;
    line-height: 64px;
    width: 250px;
  }

  &:hover {
    background-color: rgba(255, 255, 255, 0.85);
    color: var(--color-black);
    text-shadow: none;
  }
`;

export const AnchorWrapper = styled.a`
  ${styles}
`;

export const ButtonWrapper = styled.a`
  ${styles}
`;
