import React from "react";
import { Box } from "./styles";

export default function ContentBox({ id, className, children, onClick }) {
  return (
    <Box id={id} onClick={onClick} className={className}>
      {children}
    </Box>
  );
}
