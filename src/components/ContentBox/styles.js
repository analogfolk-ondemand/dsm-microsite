import styled from "styled-components";
import { breakpoints } from "../../breakpoints";

export const Box = styled.div`
  align-items: center;
  border-bottom-right-radius: 60px;
  border: 1px solid var(--color-white);
  box-sizing: border-box;
  display: inline-flex;
  max-width: 480px;
  overflow: hidden;
  padding: 6px 26px 36px;
  position: relative;
  ${breakpoints.xs} {
    &:not(:last-of-type) {
      margin-right: 24px;
    }
  }
  ${breakpoints.md} {
    margin-bottom: 24px;
    max-width: 740px;
    padding: 6px 28px;
  }
  ${breakpoints.xl} {
    &:not(:last-of-type) {
      margin-right: 24px;
    }
  }
  ${breakpoints.xxl} {
    max-width: 798px;
    padding: 8px 30px;
  }
`;
