import React, { useState } from "react";
import Parse from "html-react-parser";
import { ReferencesWrapper } from "./styles";

export const References = ({ references }) => {
  const [isActive, setIsActive] = useState(false);

  const handleClickReferences = () => {
    setIsActive(!isActive);
  };

  return (
    <ReferencesWrapper
      className={`references__wrapper ${isActive ? "active" : ""}`}
    >
      <h3 className="references__title" onClick={handleClickReferences}>
        <span className="references__trigger">{isActive ? "-" : "+"}</span>
        &nbsp;References
      </h3>

      <div className={`references__content ${isActive ? "active" : ""}`}>
        {references.map((item, index) => {
          return <p key={index}>{Parse(item.reference)}</p>;
        })}
      </div>
    </ReferencesWrapper>
  );
};
