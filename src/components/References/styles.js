import styled from "styled-components";
import { breakpoints } from "../../breakpoints";

export const ReferencesWrapper = styled.div`
  background-color: var(--color-lighterGrey);
  height: 65px;
  position: relative;
  top: 1px;
  width: 100%;
  ${breakpoints.xs} {
    height: 60px;
    margin-top: 90px;
  }
  ${breakpoints.xlOnly} {
    margin-top: 10px;
    transition: top 0.15s;

    &.active {
      top: -108px;
    }
  }
  ${breakpoints.xl} {
    margin-bottom: -38px;
    padding: 10px 0;
    top: -30px;
  }

  .references__title {
    background-color: var(--color-lighterGrey);
    color: var(--color-blue);
    cursor: pointer;
    font-size: 1.25rem;
    line-height: 50px;
    margin: 0;
    padding-left: 20px;
    position: relative;
    z-index: 3;
    ${breakpoints.md} {
      padding-left: 44px;
    }

    .references__trigger {
      color: inherit;
      display: inline-block;
      font-size: 2.8rem;
      font-weight: 500;
      margin-right: 6px;
      position: relative;
      top: 8px;
      width: 23px;
    }
  }

  .references__content {
    background-color: var(--color-lighterGrey);
    opacity: 0;
    padding-left: 20px;
    padding-right: 40px;
    position: relative;
    top: -50px;
    transition: all 0.5s;
    z-index: -1;
    pointer-events: none;
    ${breakpoints.md} {
      padding-left: 79px;
    }

    &.active {
      opacity: 1;
      padding-bottom: 20px;
      padding-top: 10px;
      top: 0;
      z-index: 2;
      pointer-events: initial;
    }

    p {
      color: var(--color-lightDarkGrey);
      font-size: 0.9rem;
      letter-spacing: -0.035em;
      line-height: 22px;
      margin: 0;
      word-break: break-all;
    }
  }
`;
