import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import HoverBox from "../HoverBox/HoverBox";

export const ClickableBoxWrapper = styled.a`
  text-decoration: none;
`;

export const ClickableBoxDiv = styled(HoverBox)`
  ${breakpoints.xxl} {
    height: 330px;
    padding: 26px 26px 8px;

    @media screen and (min-height: 800px) and (max-height: 1023.99px) {
      height: 300px;
      padding: 20px 24px 4px;
    }
  }

  &.active,
  &:hover {
    br {
      display: none;
    }

    .box-hover__content {
      ${breakpoints.xxl} {
        font-size: 1rem;
      }
    }

    .box-hover__trigger {
      opacity: 1;
      img {
        filter: invert(46%) sepia(96%) saturate(2402%) hue-rotate(170deg)
          brightness(97%) contrast(101%);
      }
    }
  }

  .box-hover__title {
    ${breakpoints.xlOnly} {
      font-size: 2.4rem;
      line-height: 46px;
    }
  }

  .box-hover__trigger {
    transition: all 0.5s;
    bottom: 0;

    img {
      width: 46px;

      ${breakpoints.mdDown} {
        filter: invert(46%) sepia(96%) saturate(2402%) hue-rotate(170deg)
          brightness(97%) contrast(101%);

        path {
          fill: blue;
        }
      }
    }
  }
`;
