import React from "react";
import { ClickableBoxDiv, ClickableBoxWrapper } from "./styles";

export default function ClickableBox({ id, title, content, linkURL, triggerIcon, children }) {
  return (
    <ClickableBoxWrapper
      id={id ? id : ""}
      className="clickable-box"
      href={linkURL}
      target="_blank"
    >
      <ClickableBoxDiv
        title={title}
        content={content}
        triggerIcon={triggerIcon}
      />
    </ClickableBoxWrapper>
  );
}