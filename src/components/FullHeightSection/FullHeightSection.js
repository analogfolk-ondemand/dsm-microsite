import React, { useState, useContext } from "react";
import Parse from "html-react-parser";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { PageDataContext } from "../../App";
import { VisibilityObserver } from "reactjs-visibility";
import { FullHeightWrapperOuter, FullHeightWrapper } from "./styles";
import Button from "../../components/Button/Button";

export default function FullHeightSection({
  children,
  className,
  contactButtonLink,
  contactButtonText,
  content,
  contentAnimation,
  contentDelay,
  haveHeader,
  headerLogoAlt,
  headerLogoLink,
  headerLogoSrc,
  id,
  image,
  imageAlt,
  imageBG,
  imageMobile,
  poster,
  printH1,
  style,
  subTitle,
  subTitleAnimation,
  subTitleDelay,
  title,
  titleAnimation,
  titleDelay,
  twoColumns,
  video,
}) {
  const { isMobile } = useContext(PageDataContext);
  const [headerVisible, setheaderVisible] = useState(false);

  const handleChangeVisibility = (visible) => {
    if (visible) {
      setTimeout(() => {
        setheaderVisible(true);
      }, isMobile ? 0 : 1400);
    } else {
      setheaderVisible(false);
    }
  };

  if (twoColumns) {
    return (
      <FullHeightWrapperOuter
        className="fullheight-section"
        style={style}
        id={id}
      >
        <FullHeightWrapper
          className={`full-h-section two-columns ${className}`}
        >
          <div className="full-h-section__column full-h-section__column--left">
            {imageBG && (
              // <AnimationOnScroll animateIn="animate__fadeIn" delay={500}>
              <figure>
                <img
                  src={imageBG}
                  className={`column-left-img bg-img`}
                  alt={imageAlt ? imageAlt : "section image"}
                />
              </figure>
              // </AnimationOnScroll>
            )}

            {image && (
              <AnimationOnScroll
                animateIn="animate__fadeInUp"
                delay={isMobile ? 0 : 1600}
              >
                <img
                  src={image}
                  className={`column-left-img d-none d-md-block desktop-img`}
                  alt={imageAlt ? imageAlt : "section image"}
                />
              </AnimationOnScroll>
            )}
            {imageMobile && (
              <AnimationOnScroll
                animateIn="animate__fadeInUp"
                delay={isMobile ? 0 : 1600}
              >
                <img
                  src={imageMobile}
                  className="column-left-img d-md-none mobile-img"
                  alt={imageAlt ? imageAlt : "section image"}
                />
              </AnimationOnScroll>
            )}

            {children}
          </div>

          <div className="full-h-section__column full-h-section__column--right">
            <div className="full-h-section__column-inner">
              {title && (
                <AnimationOnScroll
                  animateIn={
                    titleAnimation ? titleAnimation : "animate__fadeInUp"
                  }
                  delay={titleDelay ? titleDelay : 0}
                >
                  <h2 className="full-h-section__column-title">
                    {Parse(title)}
                  </h2>
                </AnimationOnScroll>
              )}
              {subTitle && (
                <AnimationOnScroll
                  animateIn={
                    subTitleAnimation ? subTitleAnimation : "animate__fadeInUp"
                  }
                  delay={subTitleDelay ? subTitleDelay : 0}
                >
                  <p className="full-h-section__column-content subtitle">
                    <strong>{Parse(subTitle)}</strong>
                  </p>
                </AnimationOnScroll>
              )}
              {content && (
                <AnimationOnScroll
                  animateIn={
                    contentAnimation ? contentAnimation : "animate__fadeInUp"
                  }
                  delay={contentDelay ? contentDelay : 0}
                >
                  <p className="full-h-section__column-content content">
                    {Parse(content)}
                  </p>
                </AnimationOnScroll>
              )}
            </div>
          </div>
        </FullHeightWrapper>
      </FullHeightWrapperOuter>
    );
  }

  return (
    <FullHeightWrapperOuter
      className="fullheight-section"
      style={style}
      id={id}
    >
      {video && (
        <video className="bg-video" autoPlay muted loop poster={poster}>
          <source src={video} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      )}

      {haveHeader && (
        <VisibilityObserver onChangeVisibility={handleChangeVisibility}>
          <section
            className={`head-section animate__animated ${
              headerVisible ? "animate__fadeInUp" : ""
            }`}
          >
            <a
              id="header_logo"
              href={headerLogoLink}
              target="_blank"
              className="main-logo"
              rel="noreferrer"
            >
              <img src={headerLogoSrc} alt={headerLogoAlt} />
            </a>

            <Button
              id="contact_button"
              className="contact-button"
              link={contactButtonLink}
              text={contactButtonText}
            />
          </section>
        </VisibilityObserver>
      )}

      <FullHeightWrapper className={`full-h-section ${className}`}>
        {title && (
          <AnimationOnScroll
            animateIn={titleAnimation ? titleAnimation : "animate__fadeInUp"}
            delay={titleDelay ? titleDelay : 0}
          >
            {printH1 ? (
              <h1 className="full-h-section__title">{Parse(title)}</h1>
            ) : (
              <h2 className="full-h-section__title">{Parse(title)}</h2>
            )}
          </AnimationOnScroll>
        )}

        {subTitle && (
          <AnimationOnScroll
            animateIn={
              subTitleAnimation ? subTitleAnimation : "animate__fadeInUp"
            }
            delay={subTitleDelay ? subTitleDelay : 0}
          >
            <p className="full-h-section__subTitle">
              <strong>{Parse(subTitle)}</strong>
            </p>
          </AnimationOnScroll>
        )}

        {content && (
          <AnimationOnScroll
            animateIn={
              contentAnimation ? contentAnimation : "animate__fadeInUp"
            }
            delay={contentDelay ? contentDelay : 0}
          >
            <p className="full-h-section__content">{Parse(content)}</p>
          </AnimationOnScroll>
        )}

        {children}
      </FullHeightWrapper>
    </FullHeightWrapperOuter>
  );
}
