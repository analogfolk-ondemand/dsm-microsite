import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import { HoverBoxWrapperStyles } from "../HoverBox/styles";

export const FullHeightWrapperOuter = styled.div`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  overflow: hidden;
  position: relative;
  width: 100%;
  ${breakpoints.md} {
    scroll-snap-align: start;
  }

  .bg-video {
    height: 100%;
    left: 0;
    object-fit: cover;
    position: absolute;
    top: 0;
    width: 100%;
    z-index: -1;
  }

  .head-section {
    align-items: flex-end;
    box-sizing: border-box;
    display: flex;
    justify-content: space-between;
    left: 0;
    margin: 0 auto;
    opacity: 0;
    padding: 0 20px;
    position: relative;
    right: 0;
    top: 16px;
    transition: all 0.5s;
    width: 100%;
    z-index: 5;
    ${breakpoints.xl} {
      max-width: 1920px;
      top: 58px;
    }
    ${breakpoints.md} {
      padding: 0 50px;
    }
    ${breakpoints.lg} {
      align-items: flex-start;
    }

    &.header-visible {
      opacity: 1;
      top: 16px;
    }

    .main-logo {
      display: block;
      height: 46px;
      width: 98px;
      ${breakpoints.md} {
        height: 84px;
        position: relative;
        top: -4px;
        width: 160px;
      }

      img {
        width: 100%;
        height: 100%;
      }
    }

    .contact-button {
      width: 139px;
      height: 34px;
      ${breakpoints.xs} {
        font-size: 0.75rem;
        line-height: 34px;
      }
      ${breakpoints.md} {
        height: 64px;
        width: 250px;
      }
    }
  }
`;

export const FullHeightWrapper = styled.div`
  align-content: center;
  align-items: center;
  display: flex;
  flex-direction: column;
  height: 100vh;
  justify-content: center;
  margin: 0 45px;
  padding: 0;
  position: relative;

  .full-h-section__title {
    font-size: 2.25rem;
    font-weight: 600;
    letter-spacing: -1.32px;
    line-height: 48px;
    margin: 0 0 26px;
    text-align: center;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
    ${breakpoints.xs} {
      letter-spacing: -0.88px;
    }
    ${breakpoints.md} {
      font-size: 3.3rem;
      line-height: 58px;
      margin: 0 0 34px;
    }
    ${breakpoints.xxl} {
      font-size: 3.38rem;
      margin: 0 0 40px;
      line-height: 60px;
    }
  }

  .full-h-section__content {
    font-size: 1rem;
    font-weight: 600;
    line-height: 24px;
    margin: 0 0 50px;
    text-align: center;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
    ${breakpoints.xs} {
      padding: 0 12px;
    }
    ${breakpoints.md} {
      font-size: 1.4rem;
      line-height: 32px;
      margin: 0 0 82px;
    }
    ${breakpoints.xxl} {
      font-size: 1.44rem;
      line-height: 34px;
      margin: 0 0 100px;
    }
  }

  // two columns --------------------------------- //
  &.two-columns {
    flex-direction: row;

    .full-h-section__column {
      height: 100%;
      width: 100%;
      ${breakpoints.md} {
        width: 50%;
      }
    }

    .full-h-section__column--left {
      img {
        height: 100%;
        width: 100%;
      }
    }

    .full-h-section__column--right {
      align-items: center;
      display: flex;
      flex-direction: column;
      justify-content: center;

      .full-h-section__column-title {
        font-size: 3.3rem;
        font-weight: 600;
        letter-spacing: -1.32px;
        line-height: 60px;
        margin: 0 0 28px;
        ${breakpoints.xs} {
          margin: 47px 0;
        }
        ${breakpoints.xxl} {
          font-size: 3.38rem;
          margin: 0 0 30px;
        }
      }

      .full-h-section__column-content {
        font-size: 0.88rem;
        line-height: 24px;
        ${breakpoints.xs} {
          margin-bottom: 35px;
        }
        ${breakpoints.md} {
          font-size: 1.1rem;
          line-height: 30px;
        }
        ${breakpoints.xxl} {
          font-size: 1.13rem;
          line-height: 31px;
        }
        &.subtitle {
          margin-bottom: 47px;
          ${breakpoints.md} {
            margin-bottom: 40px;
          }
          ${breakpoints.xxl} {
            margin-bottom: 45px;
          }
        }

        strong {
          display: block;
          font-size: 1rem;
          line-height: 24px;
          ${breakpoints.md} {
            font-size: 1.4rem;
            line-height: 32px;
          }
          ${breakpoints.xxl} {
            font-size: 1.44rem;
            line-height: 34px;
          }
        }

        a {
          color: var(--color-azure);
          font-weight: 600;
          padding-left: 16px;
          text-decoration: none;
        }
      }
    }

    .full-h-section__column-inner {
      box-sizing: border-box;
      max-width: 574px;
      padding: 0 45px;
      width: 100%;
      ${breakpoints.xxl} {
        max-width: 590px;
      }
    }
  }

  // hover box wrapper styles
  ${HoverBoxWrapperStyles}
`;
