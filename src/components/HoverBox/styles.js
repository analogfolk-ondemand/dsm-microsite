import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import ContentBox from "../ContentBox/ContentBox";

const styles = `
  background-color: var(--color-white);
  box-shadow: 11px 10px 28px 0 rgba(5, 15, 58, 0.5);

  .box-hover__title {
    color: var(--color-azure);
    font-size: 1.38rem;
    line-height: 28px;
    text-shadow: none;
    ${breakpoints.md} {
      font-size: 1.6rem;
      line-height: 30px;
    }
    ${breakpoints.xxl} {
      font-size: 2rem;
      line-height: 32px;
    }

    br {
      display: none;
    }
  }
  .box-hover__content {
    opacity: 1;
    top: 20px;
    ${breakpoints.md} {
      top: 30px;
    }
    // section8
    &:not(.section-3 &) {
      ${breakpoints.mdDown} {
        font-size: 1rem;
      }
    }
  }
  .box-hover__trigger {
    &:not(.section-8 &) {
      opacity: 0;
    }
  }
`;

export const HoverBoxWrapperStyles = `
  .boxes-wrapper {
    margin: 0 auto;
    max-width: 1492px;
    width: 100%;
    // section3
    &:not(.section-8 &) {
      ${breakpoints.xs} {
        display: flex;
        overflow: auto;
      }
    }
    // section8
    &:not(.section-3 &) {
      ${breakpoints.mdDown} {
        display: flex;
        overflow: auto;
      }
    }
  }

  .boxes-wrapper-inner {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    width: 100%;
    ${breakpoints.xs} {
      flex-wrap: nowrap;
    }
    ${breakpoints.md} {
      justify-content: center;
      justify-content: space-around;
    }
    ${breakpoints.xl} {
      flex-wrap: nowrap;
    }
  }
`;

export const HoverBoxWrapper = styled(ContentBox)`
  align-items: flex-start;
  box-sizing: border-box;
  flex-direction: column;
  height: 290px;
  padding: 18px 17px 8px;
  transition: all 0.65s;
  width: 249px;
  ${breakpoints.md} {
    height: 320px;
    padding: 24px 32px 8px;
    width: 318px;
  }
  @media screen and (min-width: 1360px) and (max-width: 1400px) {
    width: 300px;

    .box-hover__title {
      font-size: 2.5rem;
      line-height: 50px;
    }
  }
  ${breakpoints.xxl} {
    height: 369px;
    padding: 26px 34px 8px;
    width: 340px;
  }

  &.active,
  &:hover,
  &:active {
    ${styles};
  }

  & > div {
    width: 100%;
  }

  .box-hover__title {
    font-size: 2.8rem;
    font-weight: 600;
    line-height: 56px;
    transition: color 0.75s;
    text-shadow: 2px 2px rgba(0, 0, 0, 0.3);
    ${breakpoints.xlOnly} {
      font-size: 2.7rem;
      line-height: 52px;
    }
    ${breakpoints.xxl} {
      font-size: 3rem;
      line-height: 58px;
    }
  }

  .box-hover__content {
    color: var(--color-black);
    font-size: 0.75rem;
    letter-spacing: -0.37px;
    line-height: 16px;
    opacity: 0;
    position: relative;
    top: 125%;
    transition: top 0.75s;
    ${breakpoints.md} {
      font-size: 0.85rem;
      letter-spacing: -0.43px;
      line-height: 21px;
    }
    ${breakpoints.xxl} {
      font-size: 0.88rem;
    }
  }

  .box-hover__trigger {
    bottom: 18px;
    box-sizing: border-box;
    height: 60px;
    left: 0px;
    padding-left: 27px;
    position: absolute;
    text-align: left;
    width: 100%;

    img {
      width: 58px;
    }
  }

  // overwrites default styles
  // mobile only
  &:not(.section-3 &) {
    ${breakpoints.mdDown} {
      ${styles};
    }
  }
  &:not(.section-8 &) {
    ${breakpoints.xs} {
      ${styles};
    }
  }
`;
