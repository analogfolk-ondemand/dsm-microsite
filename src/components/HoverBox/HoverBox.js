import React, { useState } from "react";
import Parse from "html-react-parser";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { HoverBoxWrapper } from "./styles";

import PlusIcon from "./../../assets/images/icon-plus.svg";

export default function HoverBox({ id, title, content, className, triggerIcon }) {
  const [isActive, setActive] = useState("false");

  const handleToggle = (e) => {
    if (!e.target.closest("#section-8")) {
      setActive(!isActive);
    }
  };

  return (
    <HoverBoxWrapper
      id={id ? id : ""}
      className={`box-hover ${isActive ? "" : "active"} ${
        className && className
      }`}
      onClick={handleToggle}
    >
      <div className="box-hover__title">{Parse(title)}</div>
      <div className="box-hover__content">{content && Parse(content)}</div>

      <span className="box-hover__trigger">
        <AnimationOnScroll animateIn="animate__fadeInUp" delay={400}>
          <img src={triggerIcon ? triggerIcon : PlusIcon} alt="Icon" />
        </AnimationOnScroll>
      </span>
    </HoverBoxWrapper>
  );
}
