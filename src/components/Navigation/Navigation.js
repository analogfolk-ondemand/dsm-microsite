import React, { useState } from "react";

import { NavWrapper } from "./styles";

const SECTIONS = ["", "", "", "", "", "", "", ""];

export default function Navigation({ currentSection }) {
  const [isActiveItem, setIsActiveItem] = useState(1);
  const toggleIsActiveItem = (index) => {
    setIsActiveItem(index);
  };

  const sectionClickHandler = (index, e) => {
    e && e.preventDefault();

    // eslint-disable-next-line
    const elementToView = document
      .getElementById(`section-${index}`)
      .scrollIntoView();

    toggleIsActiveItem(index);
  };

  return (
    <NavWrapper id="section_0">
      {SECTIONS.map((section, index) => {
        return (
          <li
            key={index}
            className={
              isActiveItem && currentSection === index + 1 ? "active" : ""
            }
          >
            <a
              id={`nav_item_${index + 1}`}
              href={`#section-${index + 1}`}
              aria-hidden="true"
              onClick={(e) => {
                sectionClickHandler(index + 1, e);
              }}
            >
              {section}
            </a>
          </li>
        );
      })}
    </NavWrapper>
  );
}
