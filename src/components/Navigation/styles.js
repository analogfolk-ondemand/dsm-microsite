import styled from "styled-components";
import { breakpoints } from "../../breakpoints";

export const NavWrapper = styled.ul`
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;
  position: fixed;
  right: 30px;
  top: 50%;
  transform: translateY(-50%);
  z-index: 10;
  ${breakpoints.lg2Down} {
    display: none;
  }
  @media screen and (min-width: 1599.99px) {
    right: 44px;
  }
  @media screen and (min-width: 1660px) {
    right: 79px;
  }

  li {
    background-color: rgba(255, 255, 255, 0.4);
    border-radius: 50%;
    display: inline-block;
    height: 10px;
    margin-bottom: 10px;
    width: 10px;

    &.active {
      background-color: var(--color-white);
    }

    a {
      color: transparent;
      display: block;
      height: 100%;
      text-indent: -150%;
      width: 100%;
    }
  }
`;
