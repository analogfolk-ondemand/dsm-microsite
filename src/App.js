import React, { useState, useEffect, createContext } from "react";
import PageContent from "./data/pageContent.json";
import "./App.scss";

import Navigation from "./components/Navigation/Navigation";
import SectionOne from "./sections/SectionOne/SectionOne";
import SectionTwo from "./sections/SectionTwo/SectionTwo";
import SectionThree from "./sections/SectionThree/SectionThree";
import SectionFour from "./sections/SectionFour/SectionFour";
import SectionFive from "./sections/SectionFive/SectionFive";
import SectionSix from "./sections/SectionSix/SectionSix";
import SectionSeven from "./sections/SectionSeven/SectionSeven";
import SectionEight from "./sections/SectionEight/SectionEight";

export const PageDataContext = createContext();

function App() {
  const [pageData, setPageData] = useState(null);
  const [isVisible, setIsVisible] = useState(0);
  const [isMobile, setIsMobile] = useState(false);

  const handleChangeVisibility = (visible, sectionID) => {
    if (visible) {
      setIsVisible(sectionID);
    }
  };

  //choose the screen size
  const handleResize = () => {
    if (window.innerWidth < 1025) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);
  });
  useEffect(() => {
    window.addEventListener("load", handleResize());
  }, []);

  useEffect(() => {
    setPageData(PageContent);
  }, []);

  return (
    <div className="App">
      <PageDataContext.Provider value={{ pageData, isMobile }}>
        <Navigation currentSection={isVisible} />

        <SectionOne sectionVisibility={handleChangeVisibility} />
        <SectionTwo sectionVisibility={handleChangeVisibility} />
        <SectionThree sectionVisibility={handleChangeVisibility} />
        <SectionFour sectionVisibility={handleChangeVisibility} />
        <SectionFive sectionVisibility={handleChangeVisibility} />
        <SectionSix sectionVisibility={handleChangeVisibility} />
        <SectionSeven sectionVisibility={handleChangeVisibility} />
        <SectionEight sectionVisibility={handleChangeVisibility} />
      </PageDataContext.Provider>
    </div>
  );
}

export default App;
