import React, { useState, useRef, useContext } from "react";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionEightWrapper } from "./styles";

import ClickableBox from "../../components/ClickableBox/ClickableBox";
import { References } from "../../components/References/References";

import BGSection8 from "./../../assets/images/bg-section8.jpg";
import DSMLogoDesktop from "./../../assets/images/dsm-logo-footer-desktop.png";
import DSMLogoMobile from "./../../assets/images/dsm-logo-footer-mobile.png";
import ArrowIcon from "./../../assets/images/arrow-dsm.svg";

export default function SectionEight({ sectionVisibility }) {
  const [isVisible, setIsVisible] = useState(false);
  const { pageData, isMobile } = useContext(PageDataContext);
  const footerLogoRef = useRef();
  const footerLinksRef = useRef();
  const referencesWrapperRef = useRef();
  const sectionText = pageData?.section8;

  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    if (visible) {
      setIsVisible(true);
    }
    sectionVisibility(visible, 8);
  };

  const animateFooterLinks = () => {
    if (isVisible === true) {
      setTimeout(
        () => {
          referencesWrapperRef?.current?.classList.add("animate__fadeInUp");
          footerLogoRef?.current?.classList.add("animate__fadeInUp");
          footerLinksRef?.current.parentNode.classList.add("animate__fadeInUp");
        },
        isMobile ? 0 : 1600
      );

      setIsVisible(false);
    }

    if (isVisible === false) {
      referencesWrapperRef?.current?.classList.remove("animate__fadeInUp");
      footerLogoRef?.current?.classList.remove("animate__fadeInUp");
      footerLinksRef?.current?.parentNode.classList.remove("animate__fadeInUp");
    }
  };

  return (
    <SectionEightWrapper
      id="section-8"
      className="section-8"
      style={{ backgroundImage: `url("${BGSection8}")` }}
    >
      <div className="section-8-upper">
        <div className="content-wrapper">
          <AnimationOnScroll
            animateIn="animate__fadeInUp"
            delay={isMobile ? 0 : 600}
          >
            <h2 className="full-h-section__title">{sectionText.title}</h2>
          </AnimationOnScroll>
          <AnimationOnScroll
            animateIn="animate__fadeInUp"
            delay={isMobile ? 0 : 900}
          >
            <p className="full-h-section__content">{sectionText.content}</p>
          </AnimationOnScroll>
        </div>

        <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

        <div className="boxes-wrapper">
          <div className="boxes-wrapper-inner">
            {sectionText.boxes.map((item, index) => {
              return (
                <AnimationOnScroll
                  animateIn="animate__fadeInUp"
                  key={index}
                  delay={isMobile ? 0 : 1400}
                >
                  <ClickableBox
                    id={`clickable_box_${index + 1}`}
                    title={item.title}
                    content={item.content}
                    linkURL={item.link}
                    triggerIcon={ArrowIcon}
                  />
                </AnimationOnScroll>
              );
            })}
          </div>
        </div>
      </div>

      <div
        // animateIn="animate__fadeInUp"
        // delay={isMobile ? 0 : 1000}
        ref={referencesWrapperRef}
        className="references__animation--wrapper animate__animated "
      >
        <References references={sectionText.references} />
      </div>

      <footer className="footer__section d-flex">
        <div className="footer__logos">
          <a
            id="footer_logo"
            href="https://www.dsm.com/"
            target="_blank"
            className="main-logo animate__animated"
            rel="noreferrer"
            ref={footerLogoRef}
          >
            <img src={DSMLogoMobile} className="d-md-none" alt="DSM Logo" />
            <img
              src={DSMLogoDesktop}
              className="d-none d-md-block"
              alt="DSM Logo"
            />
          </a>
        </div>

        <AnimationOnScroll animateIn={animateFooterLinks()}>
          <div className="footer__links" ref={footerLinksRef}>
            <ul>
              {sectionText.footer.links.map((item, index) => {
                return (
                  <li key={index}>
                    <a
                      id={`footer_link_${index + 1}`}
                      href={item.link}
                      target="_blank"
                      rel="noreferrer"
                    >
                      {item.text}
                    </a>
                  </li>
                );
              })}
            </ul>
            <p className="footer__copyright">{sectionText.footer.copyRight}</p>
          </div>
        </AnimationOnScroll>
      </footer>
    </SectionEightWrapper>
  );
}
