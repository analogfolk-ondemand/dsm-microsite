import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";

export const SectionEightWrapper = styled(FullHeightSection)`
  &.section-8 {
    ${breakpoints.xs} {
      margin: 0;
    }
    ${breakpoints.mdDown} {
      height: auto;
    }

    .section-8-upper {
      align-items: center;
      display: flex;
      flex-direction: column;
      height: 100%;
      justify-content: space-evenly;
      width: 100%;
    }

    .content-wrapper {
      ${breakpoints.xs} {
        margin: 0 40px;
      }
      ${breakpoints.xlOnly} {
        margin-bottom: -8px;
      }
    }

    .full-h-section__title {
      font-size: 2.25rem;
      font-weight: bold;
      letter-spacing: -0.88px;
      line-height: 48px;
      margin: 89px auto 40px;
      max-width: 660px;
      ${breakpoints.md} {
        font-size: 3.44rem;
        letter-spacing: -1.37px;
        line-height: 1.2;
        margin: 40px auto 24px;
      }
      ${breakpoints.xlOnly} {
        font-size: 3.38rem;
        line-height: 53px;
        margin-top: 50px;
        position: relative;
      }
      ${breakpoints.xxl} {
        font-size: 3.5rem;
        line-height: 1.23;
        margin-top: 114px;

        @media screen and (min-height: 800px) and (max-height: 1023.99px) {
          margin: 0 auto 0;
        }
      }
    }

    .full-h-section__content {
      font-size: 1rem;
      font-weight: 400;
      line-height: 24px;
      margin: 0 auto 57px;
      max-width: 818px;
      ${breakpoints.md} {
        font-size: 1.5rem;
        line-height: 25px;
      }
      ${breakpoints.xlOnly} {
        margin-bottom: 48px;
      }
      ${breakpoints.xxl} {
        font-size: 1.56rem;
        line-height: 28px;

        @media screen and (min-height: 800px) and (max-height: 1023.99px) {
          margin: 0 auto 0;
        }
      }
    }

    .boxes-wrapper {
      ${breakpoints.xs} {
        padding: 0 45px;
      }
      ${breakpoints.xlOnly} {
        margin-bottom: 10px;
        margin-top: -10px;
      }
      ${breakpoints.xxl} {
        margin: 20px auto 60px;

        @media screen and (min-height: 800px) and (max-height: 1023.99px) {
          margin: 0 auto 0;
        }
      }
    }

    .boxes-wrapper-inner {
      ${breakpoints.xs} {
        padding-right: 90px;
        & > .animate__animated {
          &:not(:last-of-type) {
            margin-right: 27px;
          }
          &:last-of-type {
            padding-right: 124px;
          }
        }
      }
      ${breakpoints.mdOnly} {
        padding-top: 20px;
      }
    }

    .clickable-box {
      &:not(:last-of-type) {
        margin-right: 27px;
      }

      .box-hover {
        ${breakpoints.xlOnly} {
          height: 260px;
        }
      }
    }

    .references__animation--wrapper {
      opacity: 0;
      width: 100%;
      z-index: 1;
      ${breakpoints.md} {
        width: calc(100% + 90px);
      }
    }

    .footer__section {
      align-items: center;
      background-color: var(--color-white);
      box-sizing: border-box;
      height: auto;
      justify-content: space-between;
      margin: 0 -45px;
      padding: 0 72px;
      width: calc(100% + 90px);
      ${breakpoints.xs} {
        flex-direction: column;
        /* margin-top: 130px; */
      }
      ${breakpoints.md} {
        height: 236px;
      }

      &.active-ref {
        /* z-index: -1; */
      }

      a,
      p {
        color: var(--color-darkGrey);
      }
    }

    .footer__logos {
      display: flex;
      align-items: center;
      ${breakpoints.xs} {
        flex-direction: column;
      }
      ${breakpoints.md} {
        align-items: flex-start;
      }

      a {
        display: inline-block;
        ${breakpoints.md} {
          &:first-of-type {
            margin-right: 40px;
          }
        }

        img {
          height: 100%;
          width: 100%;
        }
      }

      .main-logo {
        height: auto;
        opacity: 0;
        width: 204px;
        ${breakpoints.xs} {
          margin: 47px 0 59px;
        }
        ${breakpoints.md} {
          width: 244px;
        }
        ${breakpoints.xlOnly} {
          width: 182px;
        }
      }
    }

    .footer__links {
      text-align: center;
      ${breakpoints.md} {
        text-align: right;
      }
      ${breakpoints.mdOnly} {
        position: relative;
        right: 20px;
        width: 390px;
      }

      ul {
        list-style: none;
        margin: 0;
        padding: 0;
        ${breakpoints.lg} {
          padding: 46px 0 12px;
        }
        ${breakpoints.xlOnly} {
          padding-top: 25px;
        }
      }

      li {
        display: block;
        ${breakpoints.xs} {
          line-height: 34px;
        }
        ${breakpoints.md} {
          display: inline-block;
          margin-left: 24px;
        }

        a {
          text-decoration: none;
        }
      }

      .footer__copyright {
        color: var(--color-lightGrey);
        font-size: 14px;
        margin-top: 18px;
        ${breakpoints.xs} {
          margin: 40px 0 39px;
        }
      }
    }
  }
`;
