import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";

export const SectionThreeWrapper = styled(FullHeightSection)`
  &.section-3 {
    ${breakpoints.xs} {
      align-items: flex-start;
      height: 442px;
      margin: 0;
      padding-right: 88px;

      .boxes-wrapper {
        padding: 0 45px;
      }
    }

    .boxes-wrapper-inner {
      ${breakpoints.xs} {
        & > .animate__animated {
          &:not(:last-of-type) {
            margin-right: 27px;
          }
          &:last-of-type {
            padding-right: 40px;
          }
        }
      }
      ${breakpoints.mdOnly} {
        padding-top: 20px;
      }
    }
  }
`;
