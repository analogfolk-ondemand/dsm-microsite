import React, { useContext } from "react";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionThreeWrapper } from "./styles";
import HoverBox from "../../components/HoverBox/HoverBox";

import BGSection3 from "./../../assets/images/bg-section3.jpg";
import BGVidSection3 from "./../../assets/videos/bg-vid-section3.mp4";

export default function SectionThree({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const sectionText = pageData?.section3;

  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 3);
  };

  return (
    <SectionThreeWrapper
      id="section-3"
      sectionNumb={3}
      className="section-3"
      poster={BGSection3}
      video={BGVidSection3}
      sectionVisibility={sectionVisibility}
    >
      <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

      <div className="boxes-wrapper">
        <div className="boxes-wrapper-inner">
          {sectionText.boxes.map((item, index) => {
            return (
              <div id={`hover_box_${index + 1}_cta`} key={index}>
                <AnimationOnScroll
                  animateIn="animate__fadeInUp"
                  delay={isMobile ? 0 : 1400}
                >
                  <HoverBox
                    id={`hover_box_${index + 1}`}
                    title={item.title}
                    content={item.content}
                  />
                </AnimationOnScroll>
              </div>
            );
          })}
        </div>
      </div>
    </SectionThreeWrapper>
  );
}
