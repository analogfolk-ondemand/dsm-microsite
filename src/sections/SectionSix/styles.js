import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";

export const SectionSixWrapper = styled(FullHeightSection)`
  &.section-6.two-columns {
    ${breakpoints.xs} {
      height: auto;
    }
    ${breakpoints.mdDown} {
      flex-direction: column-reverse;
      margin: 0;
      text-align: center;
    }

    .full-h-section__column--left {
      align-content: center;
      box-sizing: border-box;
      display: flex;
      flex-wrap: wrap;
      justify-content: center;
      padding: 0 20px;
      width: 100%;
      ${breakpoints.mdDown} {
        align-content: flex-start;
      }
      ${breakpoints.mdOnly} {
        width: 80%;
        @media screen and (orientation: landscape) {
          width: 65%;
        }
      }
      ${breakpoints.xl} {
        width: 50%;
      }
      ${breakpoints.xxl} {
        width: 60%;
      }

      img {
        max-width: 784px;
        height: auto;
        ${breakpoints.xs} {
          width: 181px;
          height: 708px;
          margin-bottom: 90px;
        }
        ${breakpoints.xxl} {
          max-width: 824px;
        }
      }
    }

    .full-h-section__column--right {
      align-items: flex-start;
      width: 100%;
      ${breakpoints.xlOnly} {
        align-items: center;
      }
      ${breakpoints.xl} {
        width: 50%;
      }
      ${breakpoints.xxl} {
        width: 40%;
      }

      .full-h-section__column-inner {
        max-width: 612px;
        ${breakpoints.mdDown} {
          margin: 0 auto;
        }
      }

      .full-h-section__column-title {
        font-size: 2.25rem;
        line-height: 48px;
        letter-spacing: -0.88px;
        ${breakpoints.xs} {
          margin: 48px auto 54px;
        }
        ${breakpoints.mdOnly} {
          margin-top: 90px;
        }
        ${breakpoints.md} {
          font-size: 3.77rem;
          line-height: 66px;
          letter-spacing: -1.56px;
        }
        ${breakpoints.xxl} {
          font-size: 4rem;
          line-height: 69px;
        }
      }

      .full-h-section__column-content {
        font-size: 1rem;
        line-height: 24px;
        ${breakpoints.xs} {
          margin-bottom: 78px;
        }
        ${breakpoints.md} {
          font-size: 1.25rem;
          line-height: 32px;
        }
        ${breakpoints.xxl} {
          font-size: 1.44rem;
          line-height: 34px;
        }
      }
    }
  }
`;
