import React, { useContext } from "react";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionSixWrapper } from "./styles";

import BGSection6 from "./../../assets/images/bg-section6.jpg";
import Section6GraphDesktop from "./../../assets/images/section6-graph-desktop.gif";
import Section6GraphMobile from "./../../assets/images/section6-graph-mobile.png";

export default function SectionSix({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const sectionText = pageData?.section6;
  
  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 6);
  };

  return (
    <SectionSixWrapper
      twoColumns
      className="section-6"
      content={sectionText.content}
      id="section-6"
      image={Section6GraphDesktop}
      imageMobile={Section6GraphMobile}
      imageAlt={sectionText.image.alt}
      style={{ backgroundImage: `url("${BGSection6}")` }}
      title={sectionText.title}
      titleDelay={isMobile ? 0 : 900}
      contentDelay={isMobile ? 0 : 1300}
    >
      <VisibilityObserver onChangeVisibility={handleChangeVisibility} />
    </SectionSixWrapper>
  );
}
