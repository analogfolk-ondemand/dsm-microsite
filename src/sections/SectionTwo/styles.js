import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";

export const SectionTwoWrapper = styled(FullHeightSection)`
  &.section-2 {
    ${breakpoints.xs} {
      height: auto;
      padding: 49px 0 86px;
    }

    .full-h-section__content {
      max-width: 928px;
      padding: 0;
      ${breakpoints.xs} {
        letter-spacing: -0.41px;
        line-height: 28px;
      }

      span {
        ${breakpoints.lg} {
          display: inline-block;
        }
      }
    }

    .box-numeral__content-number {
      max-width: 230px;
      min-width: 230px;
      overflow: visible;

      .number {
        width: 148px;
      }
    }
  }
`;
