import React, { useContext } from "react";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionTwoWrapper } from "./styles";
import NumeralBox from "../../components/NumeralBox/NumeralBox";

import BGSection2 from "./../../assets/images/bg-section2.jpg";
import BGVidSection2 from "./../../assets/videos/bg-vid-section2.mp4";

export default function SectionTwo({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const sectionText = pageData?.section2;

  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 2);
  };

  return (
    <SectionTwoWrapper
      id="section-2"
      sectionNumb={2}
      className="section-2"
      content={sectionText.content}
      sectionVisibility={sectionVisibility}
      poster={BGSection2}
      video={BGVidSection2}
      contentDelay={isMobile ? 0 : 1400}
    >
      <AnimationOnScroll
        animateIn="animate__fadeInUp"
        delay={isMobile ? 0 : 1800}
      >
        <VisibilityObserver onChangeVisibility={handleChangeVisibility} />
        <NumeralBox
          number={sectionText.box.number}
          text={sectionText.box.text}
          animationDelay={isMobile ? 0 : 2400}
        />
      </AnimationOnScroll>
    </SectionTwoWrapper>
  );
}
