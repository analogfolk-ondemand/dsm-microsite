import React, { useContext, useEffect, useRef, useState } from "react";
// import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionOneWrapper } from "./styles";
// import Button from "../../components/Button/Button";

import BGSection1 from "./../../assets/images/bg-section1.jpg";
import BGVidSection1 from "./../../assets/videos/bg-vid-section1.mp4";
import CloseIcon from "./../../assets/images/close-cta.svg";
import DSMLogo from "./../../assets/images/dsm-logo-top.svg";

export default function SectionOne({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const [isVisible, setIsVisible] = useState(false);
  const [isVideoActive, setIsVideoActive] = useState(false);
  const watchFilmButtonRef = useRef();
  const iframeRef = useRef();
  const sectionText = pageData?.section1;

  const handleChangeVisibility = (visible) => {
    if (visible) {
      setIsVisible(true);
    }
    sectionVisibility(visible, 1);
  };

  const watchFilmHandlerClick = (e) => {
    e.preventDefault();
    const iframeSrc = iframeRef?.current;
    setIsVideoActive(!isVideoActive);

    if (isVideoActive === true) {
      setTimeout(() => {
        // this will reload the iframe
        // eslint-disable-next-line
        iframeSrc.src = iframeSrc.src;
      }, 500);
    }
  };

  useEffect(() => {
    const animateElements = () => {
      if (isVisible === true) {
        setTimeout(
          () => {
            watchFilmButtonRef?.current?.classList.add("animate__fadeInUp");
          },
          isMobile ? 100 : 1400
        );

        setIsVisible(false);
      } else {
        watchFilmButtonRef?.current?.classList.remove("animate__fadeInUp");
      }
    };
    animateElements();
  }, [isMobile, isVisible]);

  if (pageData === null) {
    return null;
  }

  return (
    <SectionOneWrapper
      id="section-1"
      className="section-1"
      title={sectionText.title}
      content={sectionText.content}
      poster={BGSection1}
      printH1={true}
      video={BGVidSection1}
      titleDelay={isMobile ? 0 : 1200}
      contentDelay={isMobile ? 0 : 1200}
      haveHeader={true}
      headerLogoLink={sectionText.logo.link}
      headerLogoSrc={DSMLogo}
      headerLogoAlt={sectionText.logo.alt}
      contactButtonLink={sectionText.contactButton.link}
      contactButtonText={sectionText.contactButton.text}
    >
      {/* <div
        className="watch-film-button-wrapper animate__animated"
        ref={watchFilmButtonRef}
      >
        <Button
          id="watch_film"
          link={sectionText.filmButton.link}
          text={sectionText.filmButton.text}
          onClick={(e) => {
            watchFilmHandlerClick(e);
          }}
        />
      </div> */}

      <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

      <div className={`watch-film-wrapper ${isVideoActive ? "active" : ""}`}>
        <div className="watch-film">
          <span
            className="watch-film-close"
            onClick={(e) => {
              watchFilmHandlerClick(e);
            }}
          >
            <img id="watch_film_close" src={CloseIcon} alt="Close Icon" />
          </span>

          <iframe
            width="98%"
            height="94%"
            src={sectionText.filmButton.src}
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowFullScreen
            ref={iframeRef}
          ></iframe>
        </div>
      </div>
    </SectionOneWrapper>
  );
}
