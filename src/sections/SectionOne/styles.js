import styled from "styled-components";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";
import { breakpoints } from "../../breakpoints";

export const SectionOneWrapper = styled(FullHeightSection)`
  &.section-1 {
    height: 100vh;
    top: -46px;
    ${breakpoints.md} {
      top: -85px;
    }

    .full-h-section__title {
      max-width: 737px;
      ${breakpoints.xs} {
        padding-top: 30px;
      }
    }

    .full-h-section__content {
      max-width: 580px;
      ${breakpoints.xs} {
        margin-bottom: 30px;
      }
      ${breakpoints.xxl} {
        max-width: 603px;
      }
    }

    .watch-film-button-wrapper {
      opacity: 0;
    }

    .watch-film-button-wrap {
      opacity: 0;
    }

    .watch-film-wrapper {
      background-color: rgba(0, 0, 0, 0.9);
      height: 100%;
      left: 0;
      opacity: 0;
      padding: 0;
      position: fixed;
      top: 0;
      transition: all 1s;
      width: 100%;
      z-index: -1;

      &.active {
        opacity: 1;
        z-index: 10;
      }
    }

    .watch-film {
      box-sizing: border-box;
      height: 100%;
      margin: auto;
      padding: 30px;
      width: 100%;
    }

    .watch-film-video {
      height: 100%;
      object-fit: cover;
      width: 100%;
    }

    .watch-film-close {
      cursor: pointer;
      display: inline-block;
      padding: 10px;
      position: absolute;
      right: -2px;
      top: 6px;
      width: 60px;
      z-index: 3;
      ${breakpoints.sm} {
        right: 28px;
      }
    }
  }
`;
