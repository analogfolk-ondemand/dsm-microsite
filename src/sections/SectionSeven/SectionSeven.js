import React, { useState, useRef, useEffect, useContext } from "react";
import Parse from "html-react-parser";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionSevenWrapper } from "./styles";

import fishSq1 from "./../../assets/videos/calculator/fish-sq-1.mp4";
import fishSq2 from "./../../assets/videos/calculator/fish-sq-2.mp4";
import fishSq3 from "./../../assets/videos/calculator/fish-sq-3.mp4";
import fishSq4 from "./../../assets/videos/calculator/fish-sq-4.mp4";
import fishSq5 from "./../../assets/videos/calculator/fish-sq-5.mp4";
import fishSq6 from "./../../assets/videos/calculator/fish-sq-6.mp4";

const algaeVideos = [
  {
    link: `${fishSq1}`,
    totalTonnes: "1",
    fishTonnes: "41.2",
    sharksAndCataceans: "42",
  },
  {
    link: `${fishSq2}`,
    totalTonnes: "10",
    fishTonnes: "412.4",
    sharksAndCataceans: "428",
  },
  {
    link: `${fishSq3}`,
    totalTonnes: "100",
    fishTonnes: "4,289",
    sharksAndCataceans: "4,124",
  },
  {
    link: `${fishSq4}`,
    totalTonnes: "500",
    fishTonnes: "20,620",
    sharksAndCataceans: "21,446",
  },
  {
    link: `${fishSq5}`,
    totalTonnes: "1000",
    fishTonnes: "41,240",
    sharksAndCataceans: "42,893",
  },
  {
    link: `${fishSq6}`,
    totalTonnes: "2000",
    fishTonnes: "82,480",
    sharksAndCataceans: "85,787",
  },
];

export default function SectionSeven({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const [activeVideo, setActiveVideo] = useState(1);
  const [isVisible, setIsVisible] = useState(false);
  const [lastVideoTonnes, setLastVideoTonnes] = useState(1);
  const totalTonnesRef = useRef();
  const lowerTonnesRef = useRef();
  const lowerNumsElsRef = useRef({});
  const sectionText = pageData?.section7;

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 7);
  };

  const handleChgVsbltyTxt = (visible) => {
    if (visible) {
      setIsVisible(true);
    }
  }

  const handleClickPrev = () => {
    if (activeVideo - 1 > 0) {
      let currentVideo = activeVideo - 1;
      setActiveVideo(currentVideo);
      handleSetLastVideoTonnes();
    }
  };

  const handleClickNext = () => {
    if (activeVideo < algaeVideos.length) {
      let currentVideo = activeVideo + 1;
      setActiveVideo(currentVideo);
      handleSetLastVideoTonnes();
    }
  };

  const handleSetLastVideoTonnes = () => {
    setLastVideoTonnes(algaeVideos[activeVideo - 1].totalTonnes);
  };

  // eslint-disable-next-line
  const animateValue = (obj, start, end, duration) => {
    let startTimestamp = null;

    const step = (timestamp) => {
      if (!startTimestamp) startTimestamp = timestamp;
      const progress = Math.min((timestamp - startTimestamp) / duration, 1);

      if (obj) {
        obj.innerHTML = Math.floor(progress * (end - start) + start);
      }
      if (progress < 1) {
        window.requestAnimationFrame(step);
      }
    };

    window.requestAnimationFrame(step);
  };

  useEffect(() => {
    const animateLowerTonnesTxt = () => {
      if (isVisible === true) {
        setTimeout(
          () => {
            lowerTonnesRef?.current?.classList.add("animate__fadeInUp");
          },
          isMobile ? 100 : 2100
        );

        setIsVisible(false);
      } else {
        lowerTonnesRef?.current?.classList.remove("animate__fadeInUp");
      }
    };
    animateLowerTonnesTxt();
  }, [isMobile, isVisible]);
  

  useEffect(() => {
    const startAnimation = parseInt(lastVideoTonnes);
    const endAnimation = parseInt(algaeVideos[activeVideo - 1].totalTonnes);

    animateValue(totalTonnesRef?.current, startAnimation, endAnimation, 400);
  }, [animateValue, activeVideo, lastVideoTonnes]);

  if (pageData === null) {
    return null;
  }

  return (
    <SectionSevenWrapper
      twoColumns
      className="section-7 row-column-reverse row-xl-reverse"
      content={sectionText.contentLeft.lowerContent}
      id="section-7"
      subTitle={sectionText.contentLeft.content}
      title={sectionText.contentLeft.title}
      titleDelay={isMobile ? 0 : 600}
      subTitleDelay={isMobile ? 0 : 900}
      contentDelay={isMobile ? 0 : 1200}
    >
      <div className="algae-calculator full-h-section__column-inner">
        <div className="algae-calculator__video-wrapper">
          {algaeVideos.map((video, index) => {
            return (
              <video
                autoPlay
                className={`algae-calculator__video animate__animated 
                  ${index === activeVideo - 1 ? "active-video" : ""}`}
                key={index}
                loop
                muted
              >
                <source src={video.link} />
              </video>
            );
          })}
        </div>

        <AnimationOnScroll
          animateIn="animate__fadeInUp"
          delay={isMobile ? 0 : 600}
        >
          <h2 className="full-h-section--title">
            {Parse(sectionText.contentRight.title)}
          </h2>
        </AnimationOnScroll>

        <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

        <AnimationOnScroll
          animateIn="animate__fadeInUp"
          delay={isMobile ? 0 : 900}
        >
          <h3 className="tonnes__title">
            <span className="tonnes__count" ref={totalTonnesRef}>
              {/* animatedValue */}
            </span>
            &nbsp;
            {activeVideo === 1
              ? Parse(`METRIC<br />TONNE`)
              : Parse(sectionText.contentRight.tonnes)}
            {/* {Parse(sectionText.contentRight.tonnes)} */}
          </h3>
        </AnimationOnScroll>

        <AnimationOnScroll
          animateIn="animate__fadeInUp"
          delay={isMobile ? 0 : 1300}
        >
          <div className="tonnes__count--controls">
            <button
              id="algae_calculator_button_less"
              className={activeVideo === 1 ? "inactive" : ""}
              onClick={handleClickPrev}
            >
              -
            </button>
            <button
              id="algae_calculator_button_more"
              className={activeVideo === 6 ? "inactive" : ""}
              onClick={handleClickNext}
            >
              +
            </button>
          </div>
        </AnimationOnScroll>

        <AnimationOnScroll
          animateIn="animate__fadeInUp"
          delay={isMobile ? 0 : 1700}
        >
          <div className="tonnes__middle">
            <p>{Parse(sectionText.contentRight.content)}</p>
          </div>
        </AnimationOnScroll>

        <VisibilityObserver onChangeVisibility={handleChgVsbltyTxt}>
          &nbsp;
        </VisibilityObserver>

        {/* <AnimationOnScroll
          animateIn="animate__fadeInUp"
          delay={isMobile ? 0 : 2100}
        > */}
        <div className="tonnes__lower animate__animated" ref={lowerTonnesRef}>
          <ul>
            {sectionText.contentRight.oceanLife.map((item, index) => {
              return (
                <li key={index}>
                  <span
                    className={`oceanlife-${index + 1}`}
                    ref={(element) =>
                      (lowerNumsElsRef.current[index] = element)
                    }
                  >
                    {index === 0
                      ? algaeVideos[activeVideo - 1].sharksAndCataceans
                      : algaeVideos[activeVideo - 1].fishTonnes}
                  </span>
                  &nbsp;
                  {item}
                </li>
              );
            })}
          </ul>
        </div>
        {/* </AnimationOnScroll> */}
      </div>
    </SectionSevenWrapper>
  );
}
