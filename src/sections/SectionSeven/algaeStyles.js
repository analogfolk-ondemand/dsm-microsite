export const algaeStyles = `
  .algae-calculator {
    position: relative;
  }

  .algae-calculator__video-wrapper {
    z-index: 0;

    & ~ * {
      z-index: 6;
    }
  }

  .algae-calculator__video {
    &.active-video {
      opacity: 1;
      z-index: 1;
    }
    height: 100%;
    left: -16px;
    object-fit: cover;
    opacity: 0;
    position: absolute;
    top: 0;
    width: calc(100% + 31px);
    z-index: 0;
    transition: all 1.5s !important;
  }
`;
