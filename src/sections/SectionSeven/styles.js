import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";
import { algaeStyles } from "./algaeStyles";

const gradientBG = `linear-gradient(67deg, rgba(0,16,103,1) 25%, rgba(1,68,182,1) 100%)`;

export const SectionSevenWrapper = styled(FullHeightSection)`
  // START algae calculator
  ${algaeStyles}
  // END algae calculator

  &.section-7.two-columns {
    margin: 0 auto;

    ${breakpoints.mdDown} {
      height: auto;
      padding: 0;
      text-align: center;
    }

    .full-h-section__column {
      ${breakpoints.mdDown} {
        width: 100%;
      }
    }

    .full-h-section__column--left {
      align-items: center;
      display: flex;
      flex-direction: column;
      justify-content: center;
      overflow: hidden;

      .full-h-section__column-inner {
        display: flex;
        flex-direction: column;
        height: 100%;
        justify-content: space-evenly;
        max-width: none;
        text-align: center;
        padding-bottom: 40px;
        ${breakpoints.mdDown} {
          padding: 0 45px;
        }

        .full-h-section--title {
          /* opacity: 0; */
          font-size: 0.88rem;
          font-weight: 400;
          letter-spacing: -0.08px;
          line-height: 33px;
          margin: 48px 0 40px;
          ${breakpoints.xl} {
            letter-spacing: -0.12px;
          }
          ${breakpoints.xlOnly} {
            font-size: 1.29rem;
            line-height: 40px;
            margin: 40px 0 0;
          }
          ${breakpoints.xxl} {
            font-size: 1.375rem;
            line-height: 50px;
            margin: 0;
          }

          span {
            display: block;
            font-size: 2.38rem;
            font-weight: 600;
            letter-spacing: -0.21px;
            ${breakpoints.xl} {
              font-size: 3.4rem;
              letter-spacing: -0.3px;
            }
            ${breakpoints.xxl} {
              font-size: 3.5rem;
            }
          }
        }

        .tonnes__title {
          font-size: 3.63rem;
          font-weight: bold;
          letter-spacing: -0.31px;
          line-height: 64px;
          margin: 15px 0 28px;
          ${breakpoints.xl} {
            letter-spacing: -0.54px;
            margin-top: 4px;
          }
          ${breakpoints.xlOnly} {
            font-size: 5.5rem;
            line-height: 78px;
          }
          ${breakpoints.xxl} {
            line-height: 92px;
            font-size: 6.25rem;
          }

          .tonnes__text {
            display: inline-block;
          }
        }

        .tonnes__count--controls {
          margin-bottom: 20px;

          button {
            background-color: rgba(255, 255, 255, 0.27);
            border-radius: 50%;
            border: 0;
            cursor: pointer;
            display: inline-block;
            font-size: 51px;
            height: 51px;
            line-height: 51px;
            overflow: hidden;
            padding: 0;
            width: 51px;
            ${breakpoints.xl} {
              font-size: 60px;
              height: 60px;
              line-height: 60px;
              width: 60px;
            }
            ${breakpoints.xxl} {
              font-size: 66px;
              height: 66px;
              line-height: 66px;
              width: 66px;
            }

            &:first-of-type {
              margin-right: 26px;
              ${breakpoints.xxl} {
                margin-right: 29px;
              }
            }

            &:hover {
              background-color: rgba(255, 255, 255, 0.5);
            }

            &.inactive {
              cursor: initial;
              opacity: 0.5;
              pointer-events: none;
            }
          }
        }

        .tonnes__middle p {
          font-size: 1rem;
          line-height: 24px;
          ${breakpoints.mdDown} {
            margin: 34px 0 48px;
            font-size: 1.49rem;
            line-height: 26px;
          }
          ${breakpoints.xl} {
            font-size: 1.56rem;
            line-height: 28px;
          }
          ${breakpoints.xlOnly} {
            margin: 0;
          }
        }

        .tonnes__lower {
          opacity: 0;
          ${breakpoints.xl} {
            margin-bottom: 10px;
          }
          ${breakpoints.xxl} {
            margin-bottom: 20px;
          }

          ul {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            list-style: none;
            margin: 0 0 65px;
            padding: 0;
            ${breakpoints.xs} {
              margin-top: -15px;
            }
            ${breakpoints.xl} {
              margin: 0;
              padding: 0;
            }
          }

          li {
            display: inline-block;
            font-size: 1.375rem;
            font-weight: bold;
            letter-spacing: -0.54px;
            line-height: 32px;
            padding-bottom: 2px;
            text-align: center;
            white-space: nowrap;
            width: 100%;
            ${breakpoints.xl} {
              font-size: 2.48rem;
              letter-spacing: -1.08px;
              line-height: 44px;
              padding-bottom: 12px;
            }
            ${breakpoints.xxl} {
              font-size: 2.688rem;
              letter-spacing: -1.05px;
              line-height: 48px;
            }
          }
        }
      }
    }

    .full-h-section__column--right {
      background: ${gradientBG};

      .full-h-section__column-inner {
        max-width: 620px;
      }

      .full-h-section__column-title {
        font-size: 2.25rem;
        line-height: 48px;
        letter-spacing: -0.88px;
        ${breakpoints.xl} {
          font-size: 3.77rem;
          line-height: 66px;
          letter-spacing: -1.56px;
        }
        ${breakpoints.mdDown} {
          padding-top: 40px;
        }
        ${breakpoints.xxl} {
          font-size: 4rem;
          line-height: 69px;
        }
      }

      .full-h-section__column-content {
        &:first-of-type {
          ${breakpoints.mdDown} {
            padding: 0 18px;
          }
        }
        &:last-of-type {
          ${breakpoints.mdDown} {
            text-align: left;
            margin-bottom: 50px;
          }
        }
      }
    }
  }
`;
