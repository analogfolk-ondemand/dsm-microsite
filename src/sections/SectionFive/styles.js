import styled from "styled-components";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";
import { breakpoints } from "../../breakpoints";

export const SectionFiveWrapper = styled(FullHeightSection)`
  &.section-5 {
    ${breakpoints.xs} {
      height: auto;
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1399.98px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
      height: auto;
    }

    .full-h-section__column--wrapper {
      align-items: flex-end;
      display: flex;
      justify-content: flex-end;
      padding-bottom: 60px;
      width: 100%;
      ${breakpoints.mdDown} {
        align-items: center;
        flex-direction: column;
      }
    }

    .full-h-section__column {
      display: flex;
      flex-direction: column;
      justify-content: center;
      padding: 0 45px;

      &.full-h-section__column--left {
        align-items: center;
        text-align: left;
        ${breakpoints.xs} {
          max-width: 579px;
          padding: 0;
        }
        ${breakpoints.mdOnly} {
          margin-bottom: 80px;
          max-width: 554px;
          width: 100%;
        }
        ${breakpoints.xl} {
          max-width: 579px;
          padding-left: 40px;
          padding-right: 0;
          width: 44%;
        }
      }

      &.full-h-section__column--right {
        width: 64%;
        ${breakpoints.xs} {
          margin-bottom: 80px;
        }
        ${breakpoints.xl} {
          padding-left: 20px;
        }
        ${breakpoints.xlOnly} {
          padding-left: 80px;
        }
        ${breakpoints.xxl} {
          width: 57%;
        }
      }
    }

    .full-h-section--title {
      font-size: 2.25rem;
      letter-spacing: -0.88px;
      line-height: 48px;
      margin-bottom: 40px;
      ${breakpoints.mdDown} {
        text-align: center;
      }
      ${breakpoints.mdOnly} {
        margin-bottom: 60px;
      }
      ${breakpoints.md} {
        font-size: 3.38rem;
        letter-spacing: -1.32px;
        line-height: 60px;
      }
      ${breakpoints.xl} {
        margin-bottom: 20px;
      }
    }

    .full-h-section--content {
      font-size: 0.875rem;
      line-height: 24px;
      margin-bottom: 40px;
      ${breakpoints.md} {
        font-size: 1.19rem;
        line-height: 31px;
      }
    }

    .full-h-section--list {
      display: block;
      flex-wrap: wrap;
      left: -8px;
      margin-bottom: 77px;
      padding-left: 24px;
      position: relative;
      ${breakpoints.md} {
        display: flex;
        margin-bottom: 0;
      }

      li {
        box-sizing: border-box;
        display: block;
        font-size: 0.875rem;
        font-weight: 600;
        line-height: 24px;
        position: relative;
        /* white-space: nowrap; */
        ${breakpoints.md} {
          display: inline-block;
          font-size: 1rem;
          line-height: 26px;

          &:nth-child(even) {
            left: 20px;
            position: relative;
            width: 40%;
            ${breakpoints.xxl} {
              width: 35%;
            }
          }
          &:nth-child(odd) {
            padding-right: 22px;
            width: 60%;
            ${breakpoints.xxl} {
              width: 65%;
            }
          }
        }
        ${breakpoints.xxl} {
          font-size: 1.13rem;
          line-height: 28px;
        }

        &::before {
          content: "•";
          left: -16px;
          position: absolute;
          top: 1px;
        }
      }
    }

    .box-numeral {
      ${breakpoints.xs} {
        padding: 14px 15px 42px;
      }
      ${breakpoints.mdOnly} {
        max-width: 660px;
      }

      .box-numeral__content-number {
        /* min-width: 270px;

        .number {
          ${breakpoints.xs} {
            width: 182px;
          }
        } */

        max-width: 260px;
        min-width: 260px;
        overflow: visible;

        .number {
          width: 185px;
        }
      }

      .box-numeral__content-right {
        ${breakpoints.md} {
          font-size: 1.438rem;
        }
        ${breakpoints.xl} {
          width: 387px;
        }
      }
    }
  }
`;
