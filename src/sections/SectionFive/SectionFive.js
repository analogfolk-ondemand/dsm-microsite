import React, { useContext } from "react";
import Parse from "html-react-parser";
import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionFiveWrapper } from "./styles";

import NumeralBox from "../../components/NumeralBox/NumeralBox";

import BGSection5 from "./../../assets/images/bg-section5.jpg";
import BGVidSection5 from "./../../assets/videos/bg-vid-section5.mp4";

export default function SectionFive({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const sectionText = pageData?.section5;

  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 5);
  };

  return (
    <SectionFiveWrapper
      id="section-5"
      className="section-5 two-columns"
      poster={BGSection5}
      video={BGVidSection5}
    >
      <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

      <div className="full-h-section__column--wrapper">
        <div className="full-h-section__column full-h-section__column--left">
          <div className="full-h-section__column--wrapper-inner">
            <AnimationOnScroll
              animateIn="animate__fadeInUp"
              delay={isMobile ? 0 : 500}
            >
              <h2 className="full-h-section--title">
                {Parse(sectionText.title)}
              </h2>
            </AnimationOnScroll>
            <AnimationOnScroll
              animateIn="animate__fadeInUp"
              delay={isMobile ? 0 : 900}
            >
              <p className="full-h-section--content">
                {Parse(sectionText.contentLeft)}
              </p>
            </AnimationOnScroll>

            <AnimationOnScroll
              animateIn="animate__fadeInUp"
              delay={isMobile ? 0 : 1300}
            >
              <ul className="full-h-section--list">
                {sectionText.bulletList.map((item, index) => {
                  return (
                    <li key={index}>
                      <span>{item}</span>
                    </li>
                  );
                })}
              </ul>
            </AnimationOnScroll>
          </div>
        </div>

        <div className="full-h-section__column full-h-section__column--right">
          <div className="d-flex">
            <AnimationOnScroll
              animateIn="animate__fadeInUp"
              delay={isMobile ? 0 : 1700}
            >
              <NumeralBox
                number={sectionText.box.number}
                text={sectionText.box.text}
                animationDelay={isMobile ? 0 : 2000}
              />
            </AnimationOnScroll>
          </div>
        </div>
      </div>
    </SectionFiveWrapper>
  );
}
