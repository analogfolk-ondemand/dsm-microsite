import styled from "styled-components";
import { breakpoints } from "../../breakpoints";
import FullHeightSection from "../../components/FullHeightSection/FullHeightSection";

export const SectionFourWrapper = styled(FullHeightSection)`
  &.section-4.two-columns {
    ${breakpoints.xs} {
      height: auto;
    }
    ${breakpoints.mdDown} {
      flex-direction: column;
      margin: 0 auto;
      padding: 0;
    }
    ${breakpoints.lgDown} {
      justify-content: flex-start;
    }
    @media only screen and (min-device-width: 768px) and (max-device-width: 1399.98px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
      height: auto;
    }

    .full-h-section__column--left {
      position: relative;
      ${breakpoints.xl} {
        left: -45px;
      }
      ${breakpoints.mdDown} {
        width: 100%;
      }
      @media screen and (max-width: 767px) {
        height: 350px;
      }
      @media screen and (min-width: 768px) and (max-width: 1399.98px) {
        height: 260px;
        @media screen and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
          height: 390px;
        }
      }

      figure {
        margin: 0.96px;
        overflow: hidden;
        position: relative;
        width: 100%;
        ${breakpoints.mdDown} {
          height: 275px;
        }
      }

      .column-left-img {
        height: auto;
        width: 100%;
        ${breakpoints.md} {
          /* height: calc(100% - 23vh);
          max-width: calc(100% - 15.5vh); */
        }
        ${breakpoints.xxl} {
          height: 830px;
          width: 790px;
        }
      }

      .product-img-wrapper {
        position: absolute;
        ${breakpoints.xs} {
          top: 142px;
        }
        ${breakpoints.mdDown} {
          left: 0;
          padding-left: 8px;
          width: 100%;

          img {
            top: 0;
            max-width: 520px;
          }
        }
        ${breakpoints.mdOnly} {
          top: 78px;
        }
        ${breakpoints.xl} {
          bottom: 6vh;

          img {
            width: 55.6vw;
            max-width: 1086px;
          }
        }
        ${breakpoints.xxl} {
          bottom: 9vh;

          img {
            width: 57vw;
          }
        }
      }

      .product-img {
        ${breakpoints.mdDown} {
          height: auto;
          left: 0;
          margin: 0 auto;
          position: absolute;
          right: 0;
          top: 48px;
          max-width: 700px;
          width: 100%;
        }
      }
    }

    .full-h-section__column-inner {
      ${breakpoints.xlOnly} {
        max-width: 620px;
      }
    }

    .full-h-section__column--right {
      ${breakpoints.mdDown} {
        text-align: center;
      }
      ${breakpoints.mdOnly} {
        width: 80%;
      }

      .full-h-section__column-title {
        font-size: 2.25rem;
        line-height: 48px;
        letter-spacing: -0.88px;
        ${breakpoints.mdDown} {
          margin-top: 96px;
          @media screen and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 1) {
            margin-top: 20px;
          }
        }
        ${breakpoints.xlOnly} {
          font-size: 2rem;
          margin-top: 20px;
        }
        ${breakpoints.xxl} {
          font-size: 3.375rem;
          line-height: 60px;
          letter-spacing: -1.32px;
        }
      }

      .full-h-section__column-content {
        &.subtitle {
          ${breakpoints.mdDown} {
            padding: 0 20px;
          }
          ${breakpoints.xlOnly} {
            font-size: 1rem;
            line-height: 28px;
            margin-bottom: 20px;
          }
          @media screen and (min-width: 768px) and (max-width: 1399.98px) {
            margin-bottom: 50px;
            padding: 0;
          }
        }
        &.content {
          ${breakpoints.xs} {
            margin-bottom: 70px;
          }
          ${breakpoints.mdDown} {
            margin-bottom: 110px;
            text-align: left;
          }
          ${breakpoints.xlOnly} {
            font-size: 1rem;
            line-height: 28px;
            margin-bottom: 0;
          }
        }
      }
    }
  }
`;
