import React, { useContext } from "react";
// import { AnimationOnScroll } from "react-animation-on-scroll";
import { VisibilityObserver } from "reactjs-visibility";
import { PageDataContext } from "../../App";
import { SectionFourWrapper } from "./styles";

import BGPills from "../../assets/images/bg-section4-left.png";
import ProductImg from "../../assets/images/bottle-pills-desktop.png";
// import ProductImgDesktop from "../../assets/images/bottle-pills-desktop.png";

export default function SectionFour({ sectionVisibility }) {
  const { pageData, isMobile } = useContext(PageDataContext);
  const sectionText = pageData?.section4;
  const gradientBG = `linear-gradient(67deg, rgba(0,16,103,1) 25%, rgba(1,68,182,1) 100%)`;

  if (pageData === null) {
    return null;
  }

  const handleChangeVisibility = (visible) => {
    sectionVisibility(visible, 4);
  };

  return (
    <SectionFourWrapper
      twoColumns
      className="section-4"
      content={sectionText.content}
      id="section-4"
      imageBG={BGPills}
      style={{ background: gradientBG }}
      subTitle={sectionText.subTitle}
      title={sectionText.title}
      titleDelay={isMobile ? 0 : 500}
      subTitleDelay={isMobile ? 0 : 900}
      contentDelay={isMobile ? 0 : 1300}
    >
      <VisibilityObserver onChangeVisibility={handleChangeVisibility} />

      <section className="product-img-wrapper">
        <img
          src={ProductImg}
          className="product-img"
          alt={sectionText.productImg.alt}
        />
      </section>
    </SectionFourWrapper>
  );
}
